

//USERS

db.users.insertMany([
    {
        "firstName": "Luffy",
        "lastName": "Monkey",
        "email": "luffymonkey@mail.com",
        "password": "luffy01",
        "isAdmin": false
    },
    {
        "firstName": "Zoro",
        "lastName": "Roronoa",
        "email": "zorororonoa@mail.com",
        "password": "zoro01",
        "isAdmin": false
    },
    {
        "firstName": "Sanji",
        "lastName": "Vinsmoke",
        "email": "sanjivinsmoke@mail.com",
        "password": "sanji01",
        "isAdmin": false
    },
    {
        "firstName": "Robin",
        "lastName": "Nico",
        "email": "robinnico@mail.com",
        "password": "robin01",
        "isAdmin": false
    },
    {
        "firstName": "Tony",
        "lastName": "Chopper",
        "email": "tonychopper@mail.com",
        "password": "tony01",
        "isAdmin": false
    }
])

//COURSES

db.courses.insertMany([
    {
        name: "HTML",
        pice: 5000,
        isActive: false
    },
    {
        name: "CSS",
        pice: 5000,
        isActive: false
    },
    {
        name: "Javascript",
        pice: 5000,
        isActive: false
    }
])

//Read

db.users.find({"isAdmin": false})


//Update

db.users.updateOne({"firstName": "Luffy"}, {$set:{"isAdmin": true}})
db.courses.updateOne({name: "HTML"}, {$set:{isActive: true}})

//DELETE
db.courses.deleteMany({isActive:false})